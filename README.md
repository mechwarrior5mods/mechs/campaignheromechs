# Campaign Villain Mechs

Begin the campaign with a unique Centurion variant. Adds custom hero mechs for Yamata, Kane and three Black Inferno Captains to the campaign. Player receives hero mech salvage as campaign mission rewards.

### Campaign Start

* Player begins the campaign with Centurion CN9-NC hero mech instead of the CN9-D

### Mission: First Strike

* Replaces Inferno Captain's WHM-6R with WHM-INF in mission
* Replaces WHM-6R mission reward with WHM-INF

### Mission: Lock n' Load

* Replaces Kruger's MAD-3R with BLR-1E in mission
* Replaces MAD-3R mission reward with BLR-1E 

### Black Harvest

* Replaces VTR-9B with VTR-NC in mission
* Replaces VTR-9B mission reward with VTR-NC

### Mission: Killing Kane

* Replaces Kane's KGC-0000 with KGC-KANE hero mech in mission
* Replaces KGC-0000 mission reward with KGC-KANE
* Changes Kane's AI to not focus exclusively on the player. That setting made the AI easy to exploit.
* Removes Kane's firing delay
* Adds a Comstar BL-6B-KNT to Kane's lance
* Fixes a typo in the mission dialog

### Mission: Reckoning

* Replaces Yamata's ANH-1A with ANH-ROM hero mech in mission
* Adds ANH-ROM to mission rewards
* Changes Yamata's AI to not focus exclusively on the player. That setting made the AI easy to exploit.
* Removes Yamata's firing delay
* Replaces FLE-15 with HGN-732b in Yamata's lance
* Replaces FLE-15 with GRF-2N in Yamata's lance

### Instant Action

* Adds ANH-ROM, BLR-1E, CN9-CN, KGC-KANE, WHM-INF and VTR-NC to the Instant Action game mode

### Known Issues

* Hero mechs received as mission rewards won't display the correct weight and number of critical slots. To resolve this, strip the mech before refitting it.
* Hero mechs received as mission rewards may show their melee weapon slots as being empty. Saving and re-loading the game will resolve this.
* New mech variants don't appear in the Instant Action mech list when filtering by weight class. To work around this issue, select the relevant chassis from the left nav without filtering by weight class.
* Mod has not been tested in co-op mode. If you trying it in co-op, make sure all players in the party have this mod installed. However, it still may not work correctly.

## Compatibility

* Conflicts with max tonnage mods. To work around this issue, open this mod's mod.json file. Set defaultLoadOrder to 1 and save the file. That will cause this mod to load after the max tonnage mod. You'll get the effects of the max tonnage mod in all missions except the missions modified by this mod, and this mod will still work correctly.
* This mod will conflict with other mods that modify the campaign start condition.
* This mod may conflict with other mods that add new variants of the ANH, BLR, CN9, KGC, WHM or VTR
* This mod may conflict with other mods that modify the missions *First Strike*, *Lock 'n Load*, *Black Harvest*, *Killing Kane* or *Reckoning*.

## Using this mod in Mechwarrior 5

To use this mod download it from [Nexus Mods](https://www.nexusmods.com/mechwarrior5mercenaries/mods/434?tab=description&BH=0).

## Using these files in your own mod

Feel free to use these files in your own Mechwarrior 5 mods! Please give credit to the author `moddingisthegame`.

1. Purchase [Mechwarrior 5 for PC](https://www.epicgames.com/store/en-US/p/mechwarrior-5)
2. Install the [Mechwarrior 5 Modding Toolkt](https://www.epicgames.com/store/en-US/p/mechwarrior-5--mod-editor)
3. Install [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
4. Navigate to the Mechwarrior 5 Editor Plugins directory (typically C:\Program Files\Epic Games\MechWarrior5Editor\MW5Mercs\Plugins) in your terminal of choice
5. `git clone https://gitlab.com/mechwarrior5mods/mechs/campaignheromechs.git`
6. Launch the Mechwarrior 5 Modding Toolkit.
7. Click the dropdown next to Manage Mod and select `campaignheromechs`
8. Mod away!
